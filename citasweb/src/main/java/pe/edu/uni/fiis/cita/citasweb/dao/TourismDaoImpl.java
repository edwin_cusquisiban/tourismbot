package pe.edu.uni.fiis.cita.citasweb.dao;

import org.springframework.stereotype.Repository;
import pe.edu.uni.fiis.cita.citasweb.domain.Destinos;
import pe.edu.uni.fiis.cita.citasweb.domain.ListaDestinos;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;


@Repository
public class TourismDaoImpl implements TourismDAO {
    /**
     * CREATE TABLE paciente(
     nombre VARCHAR2(100),
     DNI VARCHAR2(9),
     TELEFONO VARCHAR2(13),
     CORREO VARCHAR2(100)
     );
     * @param dto
     * @return
     */
    public Destinos agregarPaciente(Destinos dto) {
        StringBuilder sbSQL = new StringBuilder();
        sbSQL.append(" INSERT INTO PACIENTE" +
                " (NOMBRE, DNI, TELEFONO,CORREO)" +
                " VALUES (?, ? ,?, ?)");
        DataSource ds = null;
        Connection connection = null;
        PreparedStatement ps = null;
        /**
         ** SELECT * FROM USUARIO WHERE CLAVE='' OR 1=1
         **/

        try {
            /*
            InitialContext context = new InitialContext();
            //Context envContext  = (Context)context.lookup("java:/comp/env");
            ds = (DataSource) context.lookup("java:/comp/env/jdbc/cita");
            connection = ds.getConnection();
            */
            connection = TourismDatasource.getConection();
            //Preparar sentencia SQL
            ps = connection.prepareStatement(sbSQL.toString());
            ps.setString(1,dto.getNombre());

            ps.execute();
            ps.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return dto;
    }


    public ListaDestinos obtenerDestinos(Destinos dto) {
        StringBuilder sbSQL = new StringBuilder();
        sbSQL.append(" SELECT NOMBRE FROM CIUDAD");
        ListaDestinos respuesta = new ListaDestinos();
        respuesta.setLista(new ArrayList<Destinos>());
        DataSource ds = null;
        Connection connection = null;
        PreparedStatement ps = null;

        try {

            connection = TourismDatasource.getConection();
            //Preparar sentencia SQL
            ps = connection.prepareStatement(sbSQL.toString());
            ResultSet rs = ps.executeQuery();
            while (rs.next()){
                Destinos destinos = new Destinos();

                destinos.setNombre(rs.getString("NOMBRE "));

                respuesta.getLista().add(destinos);
            }
            rs.close();
            ps.close();
            connection.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return respuesta;
    }
}
