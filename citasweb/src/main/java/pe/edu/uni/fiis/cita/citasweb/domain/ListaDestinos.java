package pe.edu.uni.fiis.cita.citasweb.domain;

import java.util.List;

public class ListaDestinos {
    private List<Destinos> lista;

    public List<Destinos> getLista() {
        return lista;
    }

    public void setLista(List<Destinos> lista) {
        this.lista = lista;
    }
}
