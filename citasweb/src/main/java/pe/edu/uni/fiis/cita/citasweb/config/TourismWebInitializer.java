package pe.edu.uni.fiis.cita.citasweb.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRegistration;

import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.servlet.DispatcherServlet;

public class TourismWebInitializer implements WebApplicationInitializer {

	public void onStartup(ServletContext context) throws ServletException {
		AnnotationConfigWebApplicationContext  annotationContext = new AnnotationConfigWebApplicationContext();
		annotationContext.register(TourismWebConfiguration.class);
		annotationContext.setServletContext(context);
		ServletRegistration.Dynamic registro =
				context.addServlet("dispatcher",
						new DispatcherServlet(annotationContext));
		registro.setLoadOnStartup(1);
		registro.addMapping("/servicios/*");
		
	}

}
