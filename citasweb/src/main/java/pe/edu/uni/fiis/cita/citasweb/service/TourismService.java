package pe.edu.uni.fiis.cita.citasweb.service;

import pe.edu.uni.fiis.cita.citasweb.domain.Destinos;
import pe.edu.uni.fiis.cita.citasweb.domain.ListaDestinos;

public interface TourismService {
    public Destinos agregarPaciente(Destinos dto);
    public ListaDestinos obtenerDestinos(Destinos dto);
}
