package pe.edu.uni.fiis.cita.citasweb.dao;

import pe.edu.uni.fiis.cita.citasweb.domain.Destinos;
import pe.edu.uni.fiis.cita.citasweb.domain.ListaDestinos;

public interface TourismDAO {
    public Destinos agregarPaciente(Destinos dto);
    public ListaDestinos obtenerDestinos(Destinos dto);
}
