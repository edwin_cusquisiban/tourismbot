package pe.edu.uni.fiis.cita.citasweb.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pe.edu.uni.fiis.cita.citasweb.dao.TourismDAO;
import pe.edu.uni.fiis.cita.citasweb.domain.ListaDestinos;
import pe.edu.uni.fiis.cita.citasweb.domain.Destinos;
@Service
@Transactional
public class TourismServiceImpl implements TourismService {
    @Autowired
    //@Qualifier(value = "citaDaoImpl1")
    private TourismDAO tourismDAO;
    public Destinos agregarPaciente(Destinos dto) {
        return tourismDAO.agregarPaciente(dto);
    }

    @Override
    public ListaDestinos obtenerDestinos(Destinos dto) {
        return tourismDAO.obtenerDestinos(dto);
    }

    public TourismDAO getTourismDAO() {
        return tourismDAO;
    }

    public void setTourismDAO(TourismDAO tourismDAO) {
        this.tourismDAO = tourismDAO;
    }
}
