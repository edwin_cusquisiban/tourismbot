package pe.edu.uni.fiis.cita.citasweb.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import pe.edu.uni.fiis.cita.citasweb.domain.Destinos;
import pe.edu.uni.fiis.cita.citasweb.domain.ListaDestinos;
import pe.edu.uni.fiis.cita.citasweb.service.TourismService;

@Controller
/**
 * Identificador de anotacion controller
 * Spring crea un objeto por cada thread
 **/
public class TourismController {
    @Autowired
    /**
     * Spring inyecta el objeto de tipo TourismService
     * del contexto Spring.
     * Nota: La anotacion @Service que el objeto TourismServiceImpl
     * que tiene el tipo TourismService usando polimorfismo
     */
    private TourismService tourismService;
    @RequestMapping(value = "/agregarPaciente",method = RequestMethod.POST,
        produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    /**
     * @RequestMapping: Permite el mapeo de URL con metodo del controller.
     * value: Url de mapeo del metodo
     * method: Tipo de metodo HTTP, usar POST o GET
     * produces: El formato de salida del requerimiento HTTP
     * @ResponseBody: Convierte el parametro de salida del metodo
     * de tipo Destinos a formato JSON
     *  ejemplo: {"identificador":1,"nombre":"Paolo",
     *  "dni":"812122121","telefono":"5364842","correo":"rj@uni.edu.pe"}
     *  @RequestBody: Convierte el parametro de entrada del metodo
     * de tipo Destinos a formato JSON
     *  ejemplo: {"identificador":null,"nombre":"Paolo",
     *  "dni":"812122121","telefono":"5364842","correo":"rj@uni.edu.pe"}
     */
    public @ResponseBody
    Destinos agregarPaciente(@RequestBody Destinos dto){
        return tourismService.agregarPaciente(dto);
    }

    @RequestMapping(value = "/obtenerDestinos",method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public @ResponseBody
    ListaDestinos obtenerPacientes(@RequestBody Destinos dto){
        return tourismService.obtenerDestinos(dto);
    }
}
